<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\BookingService;
use App\Services\CatalogService;
use App\Services\GuestService;
use App\Traits\ApiResponser;

class BookingController extends Controller
{
    use ApiResponser;
    public $bookingService;
    public $catalogService;
    public $guestService;

	public function __construct(BookingService $bookingService, CatalogService $catalogService, GuestService $guestService)
    {
        $this->bookingService = $bookingService;
		$this->catalogService = $catalogService;
		$this->guestService   = $guestService;
	}

    /**
     * Get All Bookings.
     *
     * @return Response Array
     */

    function booking(){
		$response = array();
		$allBooking =  $this->bookingService->getAllBooking();
        $newBookingsArr = array();
        
        foreach($allBooking->data->booking as $singleBooking){
            
            $room 	= $this->catalogService->getRoomDetail($singleBooking->room_id);
            if($room)
                $singleBooking->room 	= $room->data->rooms;
            
            $guest 	= $this->guestService->getGuestDetail($singleBooking->guest_id);
            
			if($guest){
			    $singleBooking->guest 	= $guest->data->guest;
            }
            
			$newBookingsArr[] 		= $singleBooking;
        }
        
		$response['total_bookings'] = count((array)$allBooking);
		$response['bookings'] 		= $newBookingsArr;
        return ($this->successResponse(json_encode($response), Response::HTTP_OK));
    }

    function ShowBooking($id){

        $booking = $this->bookingService->getBookingDetail($id);

        $room  = $this->catalogService->getRoomDetail($booking->data->booking->room_id);

        $booking->room  = $room->data;

		$booking->guest = $this->guestService->getGuestDetail($booking->data->booking->guest_id);

        return ($this->successResponse(json_encode($booking), Response::HTTP_OK));
    }

    public function CreateNewBooking(Request $request) {

        $existingGuest = $this->guestService->getGuestByAttr($request->all());

        if(is_null($existingGuest) || empty($existingGuest)){
            $guest_data	  = $this->guestService->createGuest($request->all());

            $guestId = $guest_data->data->guest->id;
        }
        else{
            $guestId = $existingGuest->id;
        }
        $request->request->add(['guest_id' => $guestId]);
        $response_data= $this->bookingService->createBooking($request->all());

        return ($this->successResponse(json_encode($response_data), Response::HTTP_CREATED));
    }

    function UpdateBooking(Request $request, $id){
        $response 					= array();

		$booking_response 			=  $this->bookingService->updateBooking($request->all(), $id);
		$guest_response    			= $this->guestService->updateGuest($request->only(['guest_id','guest_first_name','guest_last_name','guest_email']), $id);
		$response['booking_update'] = $booking_response;
		$response['guest_update'] 	= $guest_response;
		return ($this->successResponse($response, response::HTTP_CREATED));
    }

    public function DeleteBooking($id){
        $response_data =  $this->bookingService->deleteBooking($id);

        return ($this->successResponse(json_encode($response_data), Response::HTTP_ACCEPTED));
    }
}
