<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Services\CatalogService;
use App\Traits\ApiResponser;
use Illuminate\Support\Facades\Log;

class CatalogController extends Controller
{
    use ApiResponser;
    public $catalogService;
    
	public function __construct(CatalogService $catalogService)
    {
        $this->catalogService = $catalogService;
    }
    
    /** Controller functions for Building CRUD */

    /**
     * Get All Building.
     *
     * @return Response Array
     */
    function Buildings(){
        
        $response_data =  $this->catalogService->getAllBuilding();
        
        return ($this->successResponse(json_encode($response_data) , Response::HTTP_OK));
    }
    
    /**
     * Get Single Building.
     *
     * @return Response 
     */
    function ShowBuilding($id){
		$response_data =  $this->catalogService->getBuildingDetail($id);
		return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }
    
    /**
     * Edit Building.
     *
     * @return Response 
     */

    function UpdateBuilding(Request $request, $id){
		$response_data =  $this->catalogService->updateBuilding($request->all(), $id);

		return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }
    
    /**
     * Create Building.
     *
     * @return Request 
     * @return Response 
     */

    function CreateBuilding(Request $request){

	    if ($request->has('image')) {
            $base_url = env('BASE_URL','http://127.0.0.1:8000/');
	        $name = $base_url.$request->image->store("uploads/buildings", 'public');
	        $request['image_url'] = $name;
        }

        $response_data =  $this->catalogService->createBuilding($request->all());

        if ($response_data && $request->has('image')) {
            $request->image->store("uploads/buildings/", 'public');
        }
		return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
	}

	function DeleteBuilding($id){
		$response_data =  $this->catalogService->deleteBuilding($id);
		return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }
    
    /** Controller functions for ROOM Crud */

    /**
     * List Rooms.
     *
     * @return Response 
     */

    function Rooms(){
		$response_data =  $this->catalogService->getAllRoom();
		return ($this->successResponse(json_encode($response_data)));
	}

    /**
     * Single Room.
     *
     * @param id
     * @return Response 
     */
	function ShowRoom($id){
		$response_data =  $this->catalogService->getRoomDetail($id);
        return ($this->successResponse(json_encode($response_data)));
	}

	function EditRoom($id){
		$response_data =  $this->catalogService->getRoomDetail($id);
		return ($this->successResponse(json_encode($response_data)));
	}

	function UpdateRoom(Request $request, $id){
		$response_data =  $this->catalogService->updateRoom($request->all(), $id);
		return ($this->successResponse(json_encode($response_data), Response::HTTP_CREATED));
	}

	function CreateRoom(Request $request){
        
        //Log::debug("rooms1", array(0=>$request->all()));
		$response_data =  $this->catalogService->createRoom($request->all());
		return ($this->successResponse(json_encode($response_data), Response::HTTP_CREATED));
	}

	function DeleteRoom($id){
		$response_data =  $this->catalogService->deleteRoom($id);
		return ($this->successResponse(json_encode($response_data)));
    }
    
    /** Controller functions for Locations Crud */

    /**
     * List Locations.
     *
     * @return Response 
     */

    public function Locations() {
        $response_data =  $this->catalogService->getAllLocations();
        return ($this->successResponse(json_encode($response_data)));
    }


    public function CreateLocation(Request $request){
        $response_data =  $this->catalogService->createNewLocation($request->all());
        return ($this->successResponse(json_encode($response_data)));
    }

    public function GetLocationById($id) {
        $response_data =  $this->catalogService->getLocationById($id);
        return ($this->successResponse(json_encode($response_data)));
    }

    public function GetLocationDetails($locationId) {
        $response_data =  $this->catalogService->getLocationDetails($locationId);
        return ($this->successResponse(json_encode($response_data)));
    }

    public function UpdateLocation(Request $request, $id) {
        $response_data =  $this->catalogService->updateLocation($request->all(), $id);
        return ($this->successResponse(json_encode($response_data)));
    }

    public function DeleteLocation($id) {
        $response_data =  $this->catalogService->deleteLocation($id);
        return ($this->successResponse(json_encode($response_data)));
    }

    public function SearchLocationByName(Request $request) {
        $response_data =  $this->catalogService->searchLocationByName($request->all());
        return ($this->successResponse(json_encode($response_data)));
    } 

    /** Controller functions for Country Crud */

    /**
     * List Countries.
     *
     * @return Response 
     */

    public function getCountryList() {
        $response_data =  $this->catalogService->getAllCountries();
        return ($this->successResponse(json_encode($response_data)));
    }

    public function createCountry(Request $request) {
        $response_data =  $this->catalogService->createCountry($request->all());
        return ($this->successResponse(json_encode($response_data)));
    }

    public function getCountryById($id) {
        $response_data =  $this->catalogService->getCountryById($id);
        return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }
    public function updateCountry(Request $request, $id) {
        $response_data =  $this->catalogService->updateCountry($request->all(), $id);
        return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }

    public function deleteCountry($countryId) {
        $response_data =  $this->catalogService->deleteCountry($countryId);
        return ($this->successResponse(json_encode($response_data), Response::HTTP_OK));
    }
}
