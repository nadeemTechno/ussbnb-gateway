<?php
namespace App\Services;

use Apiz\AbstractApi;

class BookingService extends AbstractApi
{
    protected function setBaseUrl() {
      $url = config('app.url');

      return "$url:8001";  
    
    }

    protected function setPrefix () {
        return 'api/v1';
    }

    public function getAllBooking()
    {
        $bookings = $this->get('/bookings');
        $data = json_decode($bookings->getContents());

        if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 201) {
            return $data;
        }


        return null;
    }

    public function getBookingDetail($id) {
      $bookings = $this->get("/bookings/$id");

      $data = json_decode($bookings->getContents());

      if ($bookings->getStatusCode() == 200 || $bookings->getStatusCode() == 202) {
         return $data;
      }


      return null;
    }

    function createBooking(array $data){

      $guest = $this->formParams($data)->post("/bookings");

      $data = json_decode($guest->getContents());

      if ($guest->getStatusCode() == 200 || $guest->getStatusCode() == 201) {
            return $data;
      }

      return null;
    }

    public function updateBooking(array $data, $id) {

      $booking = $this->formParams($data)->put("/bookings/$id");

      $data = json_decode($booking->getContents());

      if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 201) {
            return $data;
      }

      return null;
    }

    public function deleteBooking($id) {
      $booking = $this->delete("bookings/$id");

      $data = json_decode($booking->getContents());

      if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 202) {
            return $data;
      }

      return null;
    }
}
