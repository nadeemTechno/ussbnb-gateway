<?php
namespace App\Services;

use Apiz\AbstractApi;

class CatalogService extends AbstractApi
{
    /** Base Url to connect with Catalog Module */
    protected function setBaseUrl() {
        $url = config('app.url');

        return "$url:8002";
    }

    protected function setPrefix () {
        return 'api/v1';
    }

    public function getAllBuilding(){
		$building = $this->get("/buildings");

        $data = json_decode($building->getContents());

        if ($building->getStatusCode() == 200 || $building->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }

    public function getBuildingDetail($id) {
        $building = $this->get("/buildings/$id");

        $data = json_decode($building->getContents());

        if ($building->getStatusCode() == 200 || $building->getStatusCode() == 202) {
            return $data;
        }

        return null;

    }

    public function updateBuilding(array $data, $id) {
        $booking = $this->formParams($data)->put("/buildings/$id");

        $data = json_decode($booking->getContents());

        if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }


    public function createBuilding($data) {
        $booking = $this->formParams($data)->post("/buildings");

        $data = json_decode($booking->getContents());

        if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 201) {
                return $data;
        }

        return null;

    }

    public function deleteBuilding($id) {
        $booking = $this->delete("buildings/$id");

        $data = json_decode($booking->getContents());

        if ($booking->getStatusCode() == 200 || $booking->getStatusCode() == 202) {
                return $data;
        }

        return null;
    }

    public function getRoomDetail($id)
    {
        $rooms = $this->get("/rooms/$id");
        $data = json_decode($rooms->getContents());
        if ($rooms->getStatusCode() == 200 || $rooms->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }


    public function getAllRoom() {
        $rooms = $this->get("/rooms");
        
        $data = json_decode($rooms->getContents());
           
        if ($rooms->getStatusCode() == 200 || $rooms->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }

    public function UpdateRoom($data, $id) {
        $room = $this->formParams($data)->put("/rooms/$id");

        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }

    public function createRoom($data) {
        $room = $this->formParams($data)->post("/rooms");

        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }

    public function deleteRoom($id) {
        $room = $this->delete("rooms/$id");

        $data = json_decode($room->getContents());

        if ($room->getStatusCode() == 200 || $room->getStatusCode() == 202) {
                return $data;
        }

        return null;
    }

    public function getAllLocations() {
        $locations = $this->get("/locations");

        $data = json_decode($locations->getContents());

        if ($locations->getStatusCode() == 200 || $locations->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }

    public function createNewLocation($data){
        $location = $this->formParams($data)->post("/locations");

        $data = json_decode($location->getContents());

        if ($location->getStatusCode() == 200 || $location->getStatusCode() == 201) {
                return $data;
        }

        return null;

    }

    public function getLocationById($id) {
        $localtion = $this->get("/locations/$id");

        $data = json_decode($localtion->getContents());
        if ($localtion->getStatusCode() == 200 || $localtion->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }

    public function updateLocation($data, $id) {
        $location = $this->formParams($data)->put("/locations/$id");

        $data = json_decode($location->getContents());

        if ($location->getStatusCode() == 200 || $location->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }

    public function deleteLocation($id) {
        $locations = $this->delete("locations/$id");

        $data = json_decode($locations->getContents());

        if ($locations->getStatusCode() == 200 || $locations->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }

    public function searchLocationByName($data) {
        $location = $this->formParams($data)->post("/search/location");

        $data = json_decode($location->getContents());

        if ($location->getStatusCode() == 200 || $location->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }


    public function getAllCountries() {

        $countries = $this->get("/countries");

        $data = json_decode($countries->getContents());

        if ($countries->getStatusCode() == 200 || $countries->getStatusCode() == 201) {
            return $data;
        }

        return null;
    }

    public function createCountry($data) {
        $countries = $this->formParams($data)->post("/countries");

        $data = json_decode($countries->getContents());
        //dd($countries->getStatusCode());
        if ($countries->getStatusCode() == 201 || $countries->getStatusCode() == 202) {
                return $data;
        }

        return null;
    }

    public function getCountryById($id) {
        $country = $this->get("/countries/$id");

        $data = json_decode($country->getContents());
        if ($country->getStatusCode() == 200 || $country->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }


    public function updateCountry(array $data, $id) {
        $country = $this->formParams($data)->put("/countries/$id");

        $data = json_decode($country->getContents());

        if ($country->getStatusCode() == 200 || $country->getStatusCode() == 201) {
                return $data;
        }

        return null;
    }
    public function test($data, $id) {

    }
}
