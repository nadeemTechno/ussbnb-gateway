<?php
namespace App\Services;

use Apiz\AbstractApi;

class GuestService extends AbstractApi
{
    protected function setBaseUrl() {
      $url = config('app.url');

      return "$url:8003";
    }

    protected function setPrefix () {
        return 'api/v1';
    }

    public function getGuestDetail($id)
    {
        $guest = $this->get("/guests/$id");
        $data = json_decode($guest->getContents());
        
        if ($guest->getStatusCode() == 200 || $guest->getStatusCode() == 202) {
            return $data;
        }

        return null;
    }

    public function getGuestByAttr($data) {

      $guest = $this->formParams($data)->post("/findGuestByAttr");

      $data = json_decode($guest->getContents());

      if ($guest->getStatusCode() == 200 || $guest->getStatusCode() == 202) {
            return $data;
      }

      return null;
    }

    public function createGuest(array $data){

      $guest = $this->formParams($data)->post("/guests");
      $data = json_decode($guest->getContents());

      if ($guest->getStatusCode() == 200 || $guest->getStatusCode() == 201) {
            return $data;
      }

      return null;
    }

    public function updateGuest(array $data, $id) {

      $guest = $this->formParams($data)->put("/guests/$id");

      $data = json_decode($guest->getContents());
      if ($guest->getStatusCode() == 200 || $guest->getStatusCode() == 201) {
            return $data;
      }

      return null;
    }

}
