<?php
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/api/v1/bookings', array('as'=>'Booking List', 'uses'=>'BookingController@booking'));

Route::group(['prefix' => '/v1'], function(){
    //Route::group(['prefix' => '/v1', 'middleware' =>['auth:api']], function(){
        
        //booking router
    
        Route::get('/bookings', array('as'=>'Booking List', 'uses'=>'BookingController@booking'));
        Route::post('/bookings', array('as'=>'Booking Add', 'uses'=>'BookingController@CreateNewBooking'));
        Route::get('/bookings/{id}', array('as'=>'Booking View', 'uses'=>'BookingController@ShowBooking'));
        Route::put('/bookings/{id}', array('as'=>'Booking Update', 'uses'=>'BookingController@UpdateBooking'));
        Route::delete('/bookings/{id}', array('as'=>'Booking Delete', 'uses'=>'BookingController@DeleteBooking'));
    
        Route::get('/booking/edit/{id}', array('as'=>'Booking Edit', 'uses'=>'BookingController@EditBooking'));
        Route::get('/booking/room/{id}', array('as'=>'Booking Room Wise', 'uses'=>'BookingController@roomWiseBookings'));
        Route::get('/booking/guest/{id}', array('as'=>'Guest Room Wise', 'uses'=>'BookingController@guestWiseBookings'));
    
        Route::post('/bookingByAttr', array('as'=>'Booking Search', 'uses'=>'BookingController@getBookingByAttr'));
        Route::get('/booking/status/{status}/{id}', array('as'=>'Booking Status Update', 'uses'=>'BookingController@updateBookingStatus'));
        Route::get('/booking/delete/{id}', array('as'=>'Booking Delete', 'uses'=>'BookingController@deleteBooking'));
        Route::get('/filter', array('as'=>'Booking Delete', 'uses'=>'BookingController@getBookings'));
    
        //search  booking by booking no, date, room, buildings, guests
    
        //Catalog Microservice router
        //Building
        Route::get('/buildings', array('as'=>'Building List', 'uses'=>'CatalogController@buildings'));
        Route::get('/buildings/{id}', array('as'=>'Building View', 'uses'=>'CatalogController@ShowBuilding'));
        Route::get('/building/{id}/edit', array('as'=>'Building Edit', 'uses'=>'CatalogController@EditBuilding'));
        Route::put('/buildings/{id}', array('as'=>'Building Update', 'uses'=>'CatalogController@UpdateBuilding'));
        Route::post('/buildings', array('as'=>'Building Add', 'uses'=>'CatalogController@CreateBuilding'));
        Route::delete('/buildings/{id}', array('as'=>'Building Delete', 'uses'=>'CatalogController@DeleteBuilding'));
    
    
        //Rooms
        Route::get('/rooms', array('as'=>'Room List', 'uses'=>'CatalogController@rooms'));
        Route::get('/rooms/{id}', array('as'=>'Room View', 'uses'=>'CatalogController@ShowRoom'));
        Route::get('/rooms/{id}/edit', array('as'=>'Room Edit', 'uses'=>'CatalogController@EditRoom'));
        Route::put('/rooms/{id}', array('as'=>'Room Update', 'uses'=>'CatalogController@UpdateRoom'));
        Route::post('/rooms', array('as'=>'Room Add', 'uses'=>'CatalogController@CreateRoom'));
        Route::delete('/rooms/{id}', array('as'=>'Room Delete', 'uses'=>'CatalogController@DeleteRoom'));
    
    
        //Locations
        Route::get('/locations', array('as'=>'Locations List', 'uses'=>'CatalogController@getLocations'));
        Route::post('/locations', array('as'=>'Create Location', 'uses'=>'CatalogController@createLocation'));
        Route::get('/locations/{id}', array('as'=>'Get Location', 'uses'=>'CatalogController@getLocationById'));
        Route::get('/catalog/location/detail/{id}', array('as'=>'Get Location', 'uses'=>'CatalogController@getLocationDetails'));
        Route::put('/locations/{id}', array('as'=>'Get Location', 'uses'=>'CatalogController@updateLocation'));
        Route::delete('/locations/{id}', array('as'=>'Get Location', 'uses'=>'CatalogController@deleteLocation'));
    
        Route::post('/catalog/search/location', array('as'=>'Get Location', 'uses'=>'CatalogController@SearchLocationByName'));
        //Country
        Route::get('/countries', array('as'=>'Country List', 'uses'=>'CatalogController@getCountryList'));
        Route::post('/countries', array('as'=>'Create Country', 'uses'=>'CatalogController@createCountry'));
        Route::get('/countries/{id}', array('as'=>'Get Country By Id', 'uses'=>'CatalogController@getCountryById'));
        Route::put('/countries/{id}', array('as'=>'Update Country', 'uses'=>'CatalogController@updateCountry'));
        Route::delete('/countries/{id}', array('as'=>'Delete Country', 'uses'=>'CatalogController@deleteCountry'));
        //search rooms , room availability, room wise bookings, room wise guests
    
        //Guest router
        Route::get('/guest', array('as'=>'Guest List', 'uses'=>'GuestController@guests'));
        Route::get('/guest/{id}', array('as'=>'Guest View', 'uses'=>'GuestController@ShowGuest'));
        Route::get('/guest/edit/{id}', array('as'=>'Guest Edit', 'uses'=>'GuestController@EditGuest'));
        Route::put('/guest/update', array('as'=>'Guest Update', 'uses'=>'GuestController@UpdateGuest'));
        Route::post('/guest/create', array('as'=>'Guest Add', 'uses'=>'GuestController@CreateGuest'));
        Route::get('/guest/delete/{id}', array('as'=>'Guest Delete', 'uses'=>'GuestController@DeleteGuest'));
    
    
        //search guest, guest wise bookings
    
        //Search Microservice
    
        Route::get('/booking/search/{keyword}', array('as'=>'Booking View', 'uses'=>'searchController@SearchBooking'));
    
    
    });
